package com.conferenceyh.conference.exception;

public class ReviewNotFoundException extends RuntimeException {
    public ReviewNotFoundException(String exception) {
        super(exception);
    }
}
