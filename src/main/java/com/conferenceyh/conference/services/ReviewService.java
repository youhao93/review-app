package com.conferenceyh.conference.services;

import com.conferenceyh.conference.exception.ReviewNotFoundException;
import com.conferenceyh.conference.models.Review;
import com.conferenceyh.conference.repositories.ReviewRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ReviewService implements ReviewServiceInterface {
    @Autowired
    private ReviewRepository reviewRepository;

    public List<Review> findAll() {
        return reviewRepository.findAll();
    }

    public Review findReview(Long id) {
        Optional<Review> optionalReview = reviewRepository.findById(id);

        if(optionalReview.isPresent())
            return optionalReview.get();
        else
            throw new ReviewNotFoundException("Review could not be found");
    }

    public Review saveAndFlush(Review review) {
        return reviewRepository.saveAndFlush(review);
    }


    public Review updateReview(Long id, Review review) {
        Review existingReview = reviewRepository.getOne(id);
        BeanUtils.copyProperties(review, existingReview, "id");
        return reviewRepository.saveAndFlush(existingReview);
    }

    public void deleteById(Long id) {
        Optional<Review> optionalSession = reviewRepository.findById(id);

        if(optionalSession.isPresent()) {
            reviewRepository.deleteById(id);
        } else {
            throw new ReviewNotFoundException("Session could not be deleted, because session was not found");
        }
    }

}
