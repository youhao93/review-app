package com.conferenceyh.conference.services;

import com.conferenceyh.conference.models.Review;

import java.util.List;

public interface ReviewServiceInterface {
    List<Review> findAll();

    Review findReview(Long id);

    Review saveAndFlush(Review speaker);

    Review updateReview(Long id, Review speaker);

    void deleteById(Long id);
}
