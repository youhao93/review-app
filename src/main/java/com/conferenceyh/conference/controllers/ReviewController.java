package com.conferenceyh.conference.controllers;

import com.conferenceyh.conference.exception.ReviewNotFoundException;
import com.conferenceyh.conference.models.Review;
import com.conferenceyh.conference.services.ReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import javax.xml.bind.ValidationException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/v1/reviews")
@CrossOrigin(origins = "http://localhost:4200")
public class ReviewController {

    @Autowired
    private ReviewService reviewService;

    @GetMapping
    public List<Review> list() {
        return reviewService.findAll();
    }

    @GetMapping
    @RequestMapping("{id}")
    public ResponseEntity<Review> get(@PathVariable Long id) {
        try {
            return new ResponseEntity<Review>(reviewService.findReview(id), HttpStatus.OK);
        } catch (ReviewNotFoundException exception){
           throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Review not found");
        }
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Review create(@RequestBody final Review review) throws ValidationException {
        if(review.getReview_name() != null && review.getReview_description() != null)
            return reviewService.saveAndFlush(review);
        else throw new ValidationException("Review could not be created, missing values");
    }

    @PutMapping(value ="{id}")
    public ResponseEntity<Review> updateReview(@PathVariable Long id, @RequestBody Review review) {
        try {
            return new ResponseEntity<Review>(reviewService.updateReview(id, review), HttpStatus.OK) ;
        } catch(ReviewNotFoundException exception) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Review could not be updated, because no review was found");
        }
    }

    @DeleteMapping(value = "{id}")
    public ResponseEntity<String> delete(@PathVariable Long id) {
        try {
            reviewService.deleteById(id);
            return new ResponseEntity<String>("Review is deleted", HttpStatus.OK);
        } catch(ReviewNotFoundException exception) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, exception.getMessage());
        }
    }

}
